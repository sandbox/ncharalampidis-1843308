<?php
/**
 * Facebook tab settings.
 * @TODO: Move them to drupal backend.
 */
function _facebook_tab_settings(){
	$_cache = NULL;
	
	if(!empty($_cache)){
		return $_cache;
	}

	$settings = array(
		'id' => '',
		'secret' => '',
		'status' => TRUE, // check login status
		'cookie' => TRUE, // enable cookies to allow the server to access the session
		'xfbml' => TRUE, // parse XFBML
		'unliked_path' => 'node/82',
		'liked_path' => 'fb-ekka-live/step1',
	);

	// Allow other modules to change these settings.
	drupal_alter('facebook_tab_settings', $settings);

	$_cache = $settings;

	return $settings;
}

/**
 * Facebook API class.
 */
function _facebook_tab() {
  $cache = NULL;

  if (!empty($cache)) {
    return $cache;
  }

  // Find Facebook's PHP SDK.  Use libraries API if enabled.
  $fb_lib_path = function_exists('libraries_get_path') ? libraries_get_path('facebook-php-sdk') : 'sites/all/libraries/facebook-php-sdk';
  $fb_platform = $fb_lib_path . '/src/facebook.php';

	include($fb_platform);

	$settings = _facebook_tab_settings();

  try {
    // We don't have a cached resource for this app, so we're going to create one.
    $fb = new Facebook(array(
			 'appId' => $settings['id'],
			 'secret' => isset($settings['secret']) ? $settings['secret'] : NULL,
			 'cookie' => $settings['cookie'],
		 ));

    // Cache the result, in case we're called again.
    $cache = $fb;

    return $fb;
  }
  catch (Exception $e) {
    fb_log_exception($e, t('Failed to construct Facebook client API.'));
  }
}

/**
 * Returns if the current facebook page is liked.
 */
function facebook_tab_is_liked(){
	global $fb, $sr;

	if(isset($sr['page']['liked']) and $sr['page']['liked']){
		return $sr['page']['liked'];
	}

	return FALSE;
}

/**
 * Returns the facebook app data if isset.
 */
function facebook_tab_app_data(){
	global $fb, $sr;

	if(isset($sr['app_data'])){		
		return $sr['app_data'];
	}

	return FALSE;
}

/**
 * Checks if the current drupal page is inside facebook tab.
 */
function facebook_tab_is_tab(){
	if(isset($GLOBALS['sr']) and isset($GLOBALS['sr']['page'])){
		return TRUE;
	}
	else if($_GET['q'] == 'facebook_tab' or isset($_REQUEST['signed_request'])){
		$GLOBALS['fb'] = $fb = (isset($GLOBALS['fb']))?$GLOBALS['fb']:_facebook_tab();
		$GLOBALS['sr'] = $sr = $fb->getSignedRequest();
		$GLOBALS['fb_lang'] = $sr['user']['locale'];

		if(isset($sr['page'])){
			return TRUE;
		}

		$settings = _facebook_tab_settings();
		menu_set_active_item(drupal_get_normal_path($settings['unliked_path']));
	}

	return FALSE;
}

/**
 * Returns the facebook tab protocol (http/https).
 */
function facebook_tab_protocol() {
  return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
}

/**
 * Return the facebook tab url.
 */
function facebook_tab_get_url(){
	static $_tabURL = NULL;

	if(empty($_tabURL)){
		global $fb, $sr;
		
		$settings = _facebook_tab_settings();
	
		$response = $fb->api($sr['page']['id']);
		$response['link'] = str_replace('http://', facebook_tab_protocol().'://', $response['link']);
		$_tabURL = $response['link'].'/app_'.$settings['id'];
	}

	return $_tabURL;
}

/**
 * Implements hook_init().
 */
function facebook_tab_init(){
	if(!facebook_tab_is_tab()){
		return;
	}

	global $fb, $sr;

	$settings = _facebook_tab_settings();

	$path = '';
	if(!facebook_tab_is_liked()){
		$path = $settings['unliked_path'];
	}
	else if($app_data = facebook_tab_app_data()){
		$path = $app_data['path'];
		foreach($app_data as $key => $value){
			if(in_array($key, array('path'))) continue;
			$_GET[$key] = $value;
		}
	}
	else{
		$path = $settings['liked_path'];
	}

	menu_set_active_item(drupal_get_normal_path($path));
}

/**
 * Implements hook_process_html().
 */
function facebook_tab_process_html(&$variables) {
	if(!facebook_tab_is_tab()){
		return;
	}

	global $fb;

	$base = base_path();
	$tab_url = facebook_tab_get_url();

	$patterns = array();
	$replacements = array();

	preg_match_all("|<a([^>]*)href=\"([^\"]*)\"|", $variables['page'], $matches);
	foreach($matches[2] as $link_old){
		$url = drupal_parse_url($link_old);
		$url['path'] = str_replace($base, '', $url['path']);
		if(!empty($url['path']) and !strstr($url['path'], 'http://') and !strstr($url['path'], 'https://')){
			$appdata = array(
				'app_data' => array(
					'path' => $url['path'],
				) + $url['query']
			);

			drupal_alter('facebook_tab_appdata', $appdata['app_data']);
			$patterns[] = "|<a([^>]*)href=\"".preg_quote($link_old)."\"|";
			$replacements[] = "<a $1 href=\"".$tab_url.'?'.http_build_query($appdata)."\"";
		}
		else if(empty($url['path'])){
			$patterns[] = "|<a([^>]*)href=\"".preg_quote($link_old)."\"|";
			$replacements[] = "<a $1 href=\"".$tab_url."\"";
		}
	}

	$patterns[] = "|<a([^>]*)href=\"|";
	$replacements[] = "<a $1 target=\"_top\" href=\"";

	$variables['page'] = preg_replace($patterns, $replacements, $variables['page'], -1, $count);
}

/**
 * Implements hook_form_alter().
 */
function facebook_tab_form_alter(&$form, &$form_state){
	if(!facebook_tab_is_tab()){
		return;
	}

	$form['signed_request'] = array(
		'#type' => 'hidden',
		'#value' => $_REQUEST['signed_request'],
	);
}

/**
 * Implements hook_drupal_goto_alter().
 */
function facebook_tab_drupal_goto_alter($path, $options, $http_response_code){
	if(!facebook_tab_is_tab()){
		return;
	}

	$build_url = url($path, $options);

	if ((isset($options['external']) and $options['external']) or (isset($options['absolute']) and $options['absolute'])) {
		print '<script type="text/javascript">window.top.location ="'.$build_url.'"</script>';
		exit();
	}

	$base = base_path();
	$tab_url = facebook_tab_get_url();

	$url = drupal_parse_url($build_url);
	$url['path'] = str_replace($base, '', $url['path']);
	
	$appdata = array(
		'app_data' => array(
			'path' => $url['path'],
		) + $url['query']
	);

	print '<script type="text/javascript">window.top.location ="'.$tab_url.'?'.http_build_query($appdata).'"</script>';
	exit();
}

/**
 * Implements hook_page_alter().
 */
function facebook_tab_page_alter(&$page) {
  global $fb, $fb_lang;

	$settings = _facebook_tab_settings();

	$output = '<div id="fb-root"></div>';
	$output .= '
	<script>
    // Additional JS functions here
    window.fbAsyncInit = function() {
      FB.init({
        appId: "'.$settings['id'].'", // App ID
        channelUrl: "'.url('facebook_tab/channel/'.$fb_lang, array('absolute' => TRUE)).'", // Channel File
        status: '.($settings['status']?'true':'false').', // check login status
        cookie: '.($settings['cookie']?'true':'false').', // enable cookies to allow the server to access the session
        xfbml: '.($settings['xfbml']?'true':'false').'  // parse XFBML
      });
    };

    // Load the SDK Asynchronously
    (function(d){
       var js, id = "facebook-jssdk", ref = d.getElementsByTagName("script")[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement("script"); js.id = id; js.async = true;
       js.src = "//connect.facebook.net/'.$fb_lang.'/all.js";
       ref.parentNode.insertBefore(js, ref);
     }(document));
  </script>';

  $page['page_bottom']['facebook_tab'] = array(
    '#type' => 'markup',
    '#markup' => $output,
		'#weight' => 999,
  );
}

/**
 * Implements hook_menu().
 */
function facebook_tab_menu(){
	return array(
		'facebook_tab/channel' => array(
			'page callback' => 'facebook_tab_channel_page',
			'page arguments' => array('en_US'),
			'type' => MENU_CALLBACK,
			'access callback' => TRUE,
		),
		'facebook_tab/channel/%' => array(
			'page callback' => 'facebook_tab_channel_page',
			'page arguments' => array(2),
			'type' => MENU_CALLBACK,
			'access callback' => TRUE,
		),
	);
}

/**
 * Menu callback for custom channel.
 *
 * @see http://developers.facebook.com/docs/reference/javascript/FB.init
 */
function facebook_tab_channel_page($fb_lang) {
  //headers instructing browser to cache this page.
  // Do these work?
  drupal_add_http_header("Cache-Control", "public");
  drupal_add_http_header("Expires", "Sun, 17-Jan-2038 19:14:07 GMT");

  $date = format_date(time());
	$js_sdk = facebook_tab_protocol() . "://connect.facebook.net/$fb_lang/all.js";
  $output = "<!-- Facebook Tab - $date -->\n";
  $output .= "<script src=\"".$js_sdk."\"></script>\n";
  print $output;
  exit();
}